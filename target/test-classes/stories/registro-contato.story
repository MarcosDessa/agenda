Narrativa:
Como um usuário, desejo inserir um contato na agenda

Cenário: Eu desejo inserir um contato na agenda
Dado que um contato está sendo editado
Quando informo o nome Marcos
E informo o telefone 123454321
E informo o email marcos@email.com
E envio
Então o nome registrado do contato é Marcos
E o telefone registrado do contato é 123454321
E o email registrado do contato é marcos@email.com
Dado que quero alterar o contato Marcos
Quando altero o telefone para 888888
E altero o email para novoemail@emailmassa.com
E envio novamente
Então confirmo que o novo telefone é 888888
E confirmo que o novo email é novoemail@emailmassa.com