Narrativa:
Como um usuário, desejo inserir um contato na agenda

Cenário: Eu desejo inserir um contato na agenda
Dado que um contato está sendo editado
Quando clico no botão Acessar da tela inicial
E depois clico no botão 'Novo contato'
E digito o nome Marcos
E digito o telefone 123456
E digito o email email@email.com
E clico no botão Adicionar
Então vejo que o nome registrado é Marcos
E vejo que o telefone registrado é 123456
E vejo que o email registrado é email@email.com
Quando clico no botão Editar
E troco o telefone por 888888
E troco o email para novoemail@emailmassa.com
E clico em Salvar
Então confirmo que o telefone realmente mudou para 888888
E confirmo que o email realmente mudou para novoemail@emailmassa.com
