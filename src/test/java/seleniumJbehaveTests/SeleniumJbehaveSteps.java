package seleniumJbehaveTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import testutils.TestUtils;

/*	O QUE ESTE TESTE FAZ:
 * 	- Implementa um caso de teste no qual os dados de um contato são inseridos e
 * 	  depois alterados
 * 	- O teste verifica se os dados do contato são corretamente guardados no banco
 * 	  clicando-se virtualmente nos botões e preenchendo os campos
 * 	- O teste verifica, do mesmo modo, se uma alteração é registrada no sistema
 * 	- OBS: o teste é feito diretamente sobre a interface gráfica (navegador)
 */

public class SeleniumJbehaveSteps {

	private static final long TIMEOUT_WAIT = 2;

	private static WebDriver driver;
	
	@Given("um contato está sendo editado")
	public void inicializarTudo() {
		System.setProperty("webdriver.chrome.driver", "/home/marcos/Programas/chromedriver101-0-4951-41");
		driver = new ChromeDriver();
		TestUtils.resetTabela();

		// Abre a aplicação na página index.html
		driver.get("http://localhost:8080/agenda/index.html");

	}

	@When("clico no botão Acessar da tela inicial")
	public void clicarBtnAcessar() throws InterruptedException {
		// Clica no botão Acessar -> leva ao menu "/main" (está em agenda.jsp)
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.findElement(By.id("index_acessar")).click();
	}

	@When("depois clico no botão 'Novo contato'")
	public void clicarBtnNovoContato() throws InterruptedException {
		// Clica em 'Novo contato' para adicionar um contato -> leva a novo.html
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.findElement(By.id("agenda_btn")).click();
	}

	@When("digito o nome $nome")
	public void digitarNome(String nome) throws InterruptedException {
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.findElement(By.id("novo_nome")).sendKeys(nome);
	}

	@When("digito o telefone $telefone")
	public void digitarTelefone(String telefone) throws InterruptedException {
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.findElement(By.id("novo_fone")).sendKeys(telefone);
	}

	@When("digito o email $email")
	public void digitarEmail(String email) throws InterruptedException {
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.findElement(By.id("novo_email")).sendKeys(email);
	}
	
	@When("clico no botão Adicionar")
	public void clicarBtnEnviar() throws InterruptedException {
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.findElement(By.id("novo_adicionar")).click();
	}

	@Then("vejo que o nome registrado é $nome")
	public void verificarNome(String nomeEsperado) {
		String nomeAtual = driver.findElement(By.name("nome")).getText();
		assertEquals(nomeEsperado, nomeAtual);
		
	}

	@Then("vejo que o telefone registrado é $telefone")
	public void verificarTelefone(String telefoneEsperado) {
		String telefoneAtual = driver.findElement(By.name("fone")).getText();
		assertEquals(telefoneEsperado, telefoneAtual);
	}

	@Then("vejo que o email registrado é $email")
	public void verificarEmail(String emailEsperado) throws InterruptedException {
		String emailAtual = driver.findElement(By.name("email")).getText();
		assertEquals(emailEsperado, emailAtual);
		Thread.sleep(TIMEOUT_WAIT * 1000);
		
	}
	
	@When("clico no botão Editar")
	public void clicarBtnEditar() throws InterruptedException {
		driver.findElement(By.name("editar")).click();
		Thread.sleep(TIMEOUT_WAIT * 1000);
	}
	
	@When("troco o telefone por $telefone")
	public void trocarTelefone(String telefone) throws InterruptedException {
		driver.findElement(By.name("fone")).clear();
		driver.findElement(By.name("fone")).sendKeys(telefone);
		Thread.sleep(TIMEOUT_WAIT * 1000);
	}
	
	@When("troco o email para $email")
	public void trocarEmail(String email) throws InterruptedException {
		driver.findElement(By.name("email")).clear();
		driver.findElement(By.name("email")).sendKeys(email);
		Thread.sleep(TIMEOUT_WAIT * 1000);
	}
	
	@When("clico em Salvar")
	public void salvarEditado() {
		driver.findElement(By.className("botao1")).click();
		
	}
	
	@Then("confirmo que o telefone realmente mudou para $telefone")
	public void confirmarTelefone(String telefoneEsperado) {
		String telefoneAtual = driver.findElement(By.name("fone")).getText();
		assertEquals(telefoneEsperado, telefoneAtual);
	}
	
	@Then("confirmo que o email realmente mudou para $email")
	public void confirmarEmail(String emailEsperado) throws InterruptedException {
		String emailAtual = driver.findElement(By.name("email")).getText();
		assertEquals(emailEsperado, emailAtual);
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.quit();
	}

}
