package jbehaveTests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import model.DAO;
import model.JavaBeans;
import model.ValidadorNonNull;
import testutils.TestUtils;

/*	O QUE ESTE TESTE FAZ:
 * 	--------------------
 * 
 * 	- Implementa um caso de teste no qual os dados de um contato são inseridos
 * 	- O teste verifica se os dados do contato são corretamente guardados no banco
 * 	- Verifica se uma alteração nos dados de um contato são corretamente registradas
 * 	- OBS: o teste é feito diretamente sobre a classe DAO
 */

public class InserindoContatoSteps {
	
	private String nome;
	private String telefone;
	private String email;
	private String nomeAtual;
	private String telefoneAtual;
	private String emailAtual;
	private JavaBeans contato;
	
	private DAO dao = new DAO();
	private ArrayList<JavaBeans> contatosObtidos = new ArrayList<>();
	

	@Given("um contato está sendo editado")
	public void instanciarContato() {
		TestUtils.resetTabela();
		contato = new JavaBeans();
	}
	
	@When("informo o nome $nome")
	public void informarNome(String nome) {
		this.nome = nome;
	}
	
	@When("informo o telefone $tel")
	public void informarTelefone(String tel) {
		this.telefone = tel;
	}
	
	@When("informo o email $email")
	public void informarEmail(String email) {
		this.email = email;
	}
	
	@When("envio")
	public void inserirContato() throws Exception {
		this.contato = new JavaBeans(null, nome, telefone, email);
		dao.inserirContato(contato, new ValidadorNonNull());
	}
	
	@Then("o nome registrado do contato é $nome")
	public void verificarNome(String nomeEsperado) {
		contatosObtidos = dao.listarContatos();
		nomeAtual = contatosObtidos.get(0).getNome();
		assertEquals(nomeEsperado, nomeAtual);
	}
	
	@Then("o telefone registrado do contato é $telefone")
	public void verificarTelefone(String telefoneEsperado) {
		contatosObtidos = dao.listarContatos();
		telefoneAtual = contatosObtidos.get(0).getFone();
		assertEquals(telefoneEsperado, telefoneAtual);
	}
	
	@Then ("o email registrado do contato é $email")
	public void verificarEmail(String emailEsperado) {
		this.contatosObtidos = dao.listarContatos();
		this.emailAtual = contatosObtidos.get(0).getEmail();
		assertEquals(emailEsperado, emailAtual);
	}
	
	@Given("quero alterar o contato $nome")
	public void selecionarContatoPeloNome(String nome) {
		contatosObtidos = dao.listarContatos();
		for (JavaBeans c : contatosObtidos) {
			if (c.getNome().equals(nome)) {
				this.contato = c;
				break;
			}
		}
	}
	
	@When("altero o telefone para $telefone")
	public void alterarTelefone(String telefone) {
		this.telefone = telefone;
//		this.contato.setFone(telefone);
	}
	
	@When("altero o email para $email")
	public void alterarEmail(String email) {
		this.email = email;
//		this.contato.setEmail(email);
	}
	
	@When("envio novamente")
	public void inserirContatoNovamente() {
		this.contato.setFone(telefone);
		this.contato.setEmail(email);
		dao.alterarContato(this.contato);
	}
	
	@Then("confirmo que o novo telefone é $telefone")
	public void confirmarTelefone(String telefoneEsperado) {
		contatosObtidos = dao.listarContatos();
		telefoneAtual = contatosObtidos.get(0).getFone();
		assertEquals(telefoneEsperado, telefoneAtual);
	}
	
	@Then("confirmo que o novo email é novoemail@emailmassa.com")
	public void confirmarEmail(String emailEsperado) {
		this.contatosObtidos = dao.listarContatos();
		this.emailAtual = contatosObtidos.get(0).getEmail();
		assertEquals(emailEsperado, emailAtual);
		TestUtils.resetTabela();
	}
	
}
