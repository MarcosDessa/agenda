package mockitoTests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import model.DAO;
import model.JavaBeans;
import model.ValidadorNonNull;
import testutils.TestUtils;

/*	O QUE ESTE TESTE FAZ:
 * 	- Utiliza Mockito para fazer um teste unitário na classe DAO, responsável pelo CRUD
 * 	- A classe mocada é 'ValidadorNonNull', responsável por impedir que qualquer dado nulo ou
 * 	  string vazia seja inserida no banco
 * 	- OBS: o CRUD do projeto acessa diretamente o banco de dados, por isso as classes
 * 		   do Java para manipulação do banco não foram mockadas.
 * 
 */


public class DAOmockitoTest {
	
	
	@Test
	@DisplayName("Teste unitário: cria contato e verifica se houve uso do método 'nonNull(...)'")
	public void testarDAOlistarContatos() throws Exception {
		
		// Limpando o banco de dados
		TestUtils.resetTabela();
		
		
		// Criando um contato
		JavaBeans contato = new JavaBeans();
		contato.setNome("Fulano de Tal");
		contato.setFone("12345");
		contato.setEmail("email@email");
		
		ValidadorNonNull validadorMock = Mockito.mock(ValidadorNonNull.class);
		Mockito.when(validadorMock.nonNull(
				contato.getNome(),
				contato.getFone(),
				contato.getEmail())).thenReturn(true);
		
		// Fazendo chamada ao método
		DAO dao = new DAO();
		dao.inserirContato(contato, validadorMock);
		
		// Verificando se houve chamada no método
		Mockito.verify(validadorMock);
		
		// Limpando novamente o banco de dados
		TestUtils.resetTabela();
	}
}
