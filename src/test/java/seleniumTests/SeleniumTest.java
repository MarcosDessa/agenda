package seleniumTests;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import testutils.TestUtils;

/*	O QUE ESTE TESTE FAZ:
 * 	- Implementa um caso de teste no qual os dados de um contato são inseridos e
 * 	  depois alterados
 * 	- O teste verifica se os dados do contato são corretamente guardados no banco
 * 	  clicando-se virtualmente nos botões e preenchendo os campos
 * 	- O teste verifica, do mesmo modo, se uma alteração é registrada no sistema
 */

public class SeleniumTest {
	
	private static final long TIMEOUT_WAIT = 2;
	
	private static  WebDriver driver;
	
	private String nomeEsperado;
	private String telefoneEsperado;
	private String emailEsperado;
	private String nomeAtual;
	private String telefoneAtual;
	private String emailAtual;
	
	@BeforeAll
	public static void setup() {
		System.setProperty("webdriver.chrome.driver", "/home/marcos/Programas/chromedriver101-0-4951-41");
		driver = new ChromeDriver();
		
		TestUtils.resetTabela();
	}
	
	@AfterAll
	public static void teardown() {
		TestUtils.resetTabela();
	}
	
	@Test
	@DisplayName("Inserir um contato e verificar se ele persiste no banco")
	public void testar() throws InterruptedException {
		
		// Abre a aplicação na página index.html
		driver.get("http://localhost:8080/agenda/index.html");
		
		// Clica no botão Acessar -> leva ao menu "/main" (está em agenda.jsp)
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.findElement(By.id("index_acessar")).click();
		
		// Clica em 'Novo contato' para adicionar um contato -> leva a novo.html
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.findElement(By.id("agenda_btn")).click();
		
		// Preenche os campos com os dados "esperados"
		Thread.sleep(TIMEOUT_WAIT * 1000);
		nomeEsperado = "Fulano";
		telefoneEsperado = "12345";
		emailEsperado = "email@email";
		driver.findElement(By.id("novo_nome")).sendKeys(nomeEsperado);
		driver.findElement(By.id("novo_fone")).sendKeys(telefoneEsperado);
		driver.findElement(By.id("novo_email")).sendKeys(emailEsperado);
		
		// Clica no botão 'Adicionar'
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.findElement(By.id("novo_adicionar")).click();	
		
		nomeAtual = driver.findElement(By.name("nome")).getText();
		telefoneAtual = driver.findElement(By.name("fone")).getText();
		emailAtual = driver.findElement(By.name("email")).getText();
		
		
		assertAll("Conferindo se o contato inserido está correto",
				() -> assertEquals(nomeEsperado, nomeAtual),
				() -> assertEquals(telefoneEsperado, telefoneAtual),
				() -> assertEquals(emailEsperado, emailAtual)
				);
				
		Thread.sleep(TIMEOUT_WAIT * 1000);
		
		// Clica no botão Editar
		driver.findElement(By.name("editar")).click();
		Thread.sleep(TIMEOUT_WAIT * 1000);
		
		// Muda o número do telefone
		driver.findElement(By.name("fone")).clear();
		telefoneEsperado = "888888";
		driver.findElement(By.name("fone")).sendKeys(telefoneEsperado);
		Thread.sleep(TIMEOUT_WAIT * 1000);
		
		// Muda o email
		driver.findElement(By.name("email")).clear();
		emailEsperado = "novoemail@emailmassa.com";
		driver.findElement(By.name("email")).sendKeys(emailEsperado);
		Thread.sleep(TIMEOUT_WAIT * 1000);
		
		// Clica em Salvar
		driver.findElement(By.className("botao1")).click();
		
		// Verificando se o telefone realmente mudou
		String telefoneAtual = driver.findElement(By.name("fone")).getText();
		assertEquals(telefoneEsperado, telefoneAtual);
		
		// Verificando se o email realmente mudou
		String emailAtual = driver.findElement(By.name("email")).getText();
		assertEquals(emailEsperado, emailAtual);
		Thread.sleep(TIMEOUT_WAIT * 1000);
		driver.quit();
	}
}
