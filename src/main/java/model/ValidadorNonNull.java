package model;

public class ValidadorNonNull {
	
	// Verifica se cada parâmetro não é nulo nem string vazia
	public boolean nonNull(String ... prm) {
		
		for (String i : prm) {
			if (i == null || i.equals("")) {
				return false;
			}
		}
		
		return true;
	}

}
